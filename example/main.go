package main

import (
	"fmt"
	"gitlab.com/Niesch/JuliaCore"
	"log"
	"os"
)

func main() {
	julia := juliacore.NewJulia()
	handle, err := os.Open("conf.json")
	if err != nil {
		log.Fatal(err)
	}
	err = julia.LoadConf(handle)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := julia.RespondTo("räf", "")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s, %s\n", resp.Message, resp.Type)

	resp, err = julia.RespondTo("väder", "berlin")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s, %s\n", resp.Message, resp.Type)

	resp, err = julia.RespondTo("morn", "")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s, %s\n", resp.Message, resp.Type)
}
