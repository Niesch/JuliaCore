package juliacore

import (
	"testing"
)

func TestButkus(t *testing.T) {
	res, err := Butkus()
	if err != nil {
		t.Errorf("Butkus error: %s", err.Error())
	}

	if res == "" {
		t.Errorf("got empty result")
	}
}
