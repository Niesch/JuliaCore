package juliacore

import (
	"math/rand"
)

// A StaticResponder contains a set of messages, of which it chooses one to respond with.
type StaticResponder []string

func (sr *StaticResponder) Response(trigger, target string) (Response, error) {
	resp := (*sr)[rand.Intn(len(*sr))]
	return Response{Message: resp, Type: "message"}, nil
}
