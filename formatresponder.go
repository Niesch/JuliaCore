package juliacore

import (
	"fmt"
	"math/rand"
)

// A FormatResponder takes one of its Responses and attempts to format-string the target into it.
// Like with CombinationResponder, the MessageType field indicates what type the response should be.
type FormatResponder struct {
	Responses   []string `json:"responses"`
	MessageType string   `json:"messagetype"`
}

func (fr *FormatResponder) Response(str, user string) (Response, error) {
	resp := fr.Responses[rand.Intn(len(fr.Responses))]
	return Response{Message: fmt.Sprintf(resp, user), Type: fr.MessageType}, nil
}
