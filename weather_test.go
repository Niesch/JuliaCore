package juliacore

import (
	"bytes"
	"io/ioutil"
	"testing"
)

func TestGetWeatherByName(t *testing.T) {
	// Put your OWM API key in that file
	apikey, err := ioutil.ReadFile("owm_key")
	if err != nil {
		t.Errorf("Unable to read owm_key file.")
	}
	julia := NewJulia()
	conf := []byte(`{"config": {"openweathermap_apikey":"", "choice_separator": "eller"}}`)
	err = julia.LoadConf(bytes.NewReader(conf))
	if err != nil {
		t.Error(err)
	}
	julia.Conf.OWMKey = string(apikey)
	_, err = julia.GetWeatherByName("berlin")
	if err != nil {
		t.Errorf("Error from GetWeatherByName: %s", err.Error())
	}
}
