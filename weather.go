package juliacore

import (
	"fmt"
	owm "github.com/briandowns/openweathermap"
)

// Returns a string representation of the weather in location, using apikey for authenticating to OWM.
func (j *Julia) GetWeatherByName(location string) (string, error) {
	w, err := owm.NewCurrent("C", "se", j.Conf.OWMKey)
	if err != nil {
		return "", err
	}

	err = w.CurrentByName(location)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("Vädret i %s: %s, %.1f °C, %.1f m/s, %d%% luftfuktighet, %d%% molntäcke, %.1f hPa", w.Name, w.Weather[0].Description, w.Main.Temp, w.Wind.Speed, w.Main.Humidity, w.Clouds.All, w.Main.Pressure), nil
}
