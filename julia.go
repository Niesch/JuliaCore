package juliacore

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"time"
)

// Julia represents one instance of Julia.
type Julia struct {
	StaticResponders      map[string]StaticResponder      `json:"staticresponders,omitempty"`
	CombinationResponders map[string]CombinationResponder `json:"combinationresponders,omitempty"`
	FormatResponders      map[string]FormatResponder      `json:"formatresponders,omitempty"`
	Conf                  Config                          `json:"config"`
}

// A Responder provides a Response based on trigger and target.
type Responder interface {
	Response(trigger, target string) (Response, error)
}

// Response represents a response with a specified Type.
type Response struct {
	Message string
	Type    string
}

// Config represents the configurable options a user might care about.
type Config struct {
	OWMKey    string `json:"openweathermap_apikey"`
	ChoiceSep string `json:"choice_separator"`
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

// Returns a blank Julia.
func NewJulia() *Julia {
	return &Julia{}
}

// Populates a Julia with the configuration found at r.
func (j *Julia) LoadConf(r io.Reader) error {
	tmp, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	return json.Unmarshal(tmp, j)
}

// Creates a new Julia and returns it populated with the configuration found at r.
func NewFromConf(r io.Reader) error {
	j := NewJulia()
	tmp, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	return json.Unmarshal(tmp, j)
}

// Returns a suitable reply to the trigger/target combination.
func (j *Julia) RespondTo(trigger, target string) (Response, error) {
	switch trigger {
	case "hjälp", "help":
		msg := "Tillgängliga alternativ: väder|weather <plats>, bestäm|välj <eller-separerad lista>, butkus, "
		for k := range j.StaticResponders {
			msg = msg + ", " + k
		}

		for k := range j.FormatResponders {
			msg = msg + ", " + k + " <argument>"
		}

		for k, r := range j.CombinationResponders {
			if r.TargetIndex < 0 {
				msg = msg + ", " + k
			} else {
				msg = msg + ", " + k + " <argument>"
			}
		}
		return Response{msg, "message"}, nil
	case "väder", "weather":
		weather, err := j.GetWeatherByName(target)
		if err != nil {
			return Response{}, ErrNoWeather
		}
		return Response{weather, trigger}, nil
	case "bestäm", "välj":
		return Response{j.Choose(target), "message"}, nil
	case "butkus":
		resp, err := Butkus()
		if err != nil {
			return Response{}, err
		}
		return Response{resp, "message"}, nil
	}

	sr, ok := j.StaticResponders[trigger]
	if ok {
		return sr.Response(trigger, target)
	}

	cr, ok := j.CombinationResponders[trigger]
	if ok {
		if cr.TargetIndex >= 0 && target == "" {
			return Response{}, errors.New(fmt.Sprintf("%s behöver argument", trigger))
		}
		return cr.Response(trigger, target)
	}

	fr, ok := j.FormatResponders[trigger]
	if ok {
		if target == "" {
			return Response{}, errors.New(fmt.Sprintf("%s behöver argument", trigger))
		}
		return fr.Response(trigger, target)
	}

	return Response{}, ErrNoMatch
}
