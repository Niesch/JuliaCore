package juliacore

import (
	"bytes"
	"io/ioutil"
	"testing"
)

func TestRespondTo(t *testing.T) {
	julia := NewJulia()
	conf := []byte(`{
    "config": {
	"openweathermap_apikey": "redacted",
        "choice_separator": "eller"
    },
    "staticresponders": {
	"statictest": ["1", "2"]
    },
    "combinationresponders": {
	"combtestnouser": {
	    "responses": [
		["1"]
	    ],
	    "targetindex": -1,
	    "messagetype": "action"
	},
	"combtestuser": {
	    "responses": [
		["1"]
	    ],
	    "targetindex": 0,
	    "messagetype": "message"
	}
    },
    "formatresponders": {
	"formtest": {
	    "responses": [
		"1 %s 2"
	    ],
	    "messagetype": "action"
	}
    }
}`)
	err := julia.LoadConf(bytes.NewReader(conf))
	if err != nil {
		t.Error(err)
	}
	resp, err := julia.RespondTo("statictest", "")
	if err != nil {
		t.Error(err)
	}
	if resp.Message != "1" && resp.Message != "2" {
		t.Errorf("resp.Message was %s, expected 1 or 2", resp.Message)
	}

	resp, err = julia.RespondTo("combtestnouser", "asdf")
	if err != nil {
		t.Error(err)
	}
	if resp.Message != "1" {
		t.Errorf("resp.Message was %s, expected 1", resp.Message)
	}

	resp, err = julia.RespondTo("combtestuser", "asdf")
	if err != nil {
		t.Error(err)
	}
	if resp.Message != "asdf 1" {
		t.Errorf("resp.Message was %s, expected asdf 1", resp.Message)
	}

	resp, err = julia.RespondTo("formtest", "asdf")
	if err != nil {
		t.Error(err)
	}
	if resp.Message != "1 asdf 2" {
		t.Errorf("resp.Message was %s, expected 1 asdf 2", resp.Message)
	}

	// Put your OWM API key in that file
	apikey, err := ioutil.ReadFile("owm_key")
	if err != nil {
		t.Errorf("Unable to read owm_key file.")
	}
	julia.Conf.OWMKey = string(apikey)
	_, err = julia.RespondTo("väder", "berlin")
	if err != nil {
		t.Errorf("Error from RespondTo väder: %s", err.Error())
	}
	_, err = julia.RespondTo("weather", "berlin")
	if err != nil {
		t.Errorf("Error from RespondTo weather: %s", err.Error())
	}

	res, err := julia.RespondTo("bestäm", "katter eller hundar")
	if res.Message != "katter" && res.Message != "hundar" {
		t.Errorf("Error from RespondTo bestäm: got %s, expected katter or hundar", res)
	}
	if err != nil {
		t.Errorf("Error from RespondTo bestäm: %s", err.Error())
	}
	_, err = julia.RespondTo("butkus", "")
	if err != nil {
		t.Errorf("Error from RespondTo butkus: %s", err.Error())
	}
}
