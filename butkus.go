package juliacore

import (
	"io/ioutil"
	"net/http"
)

// Returns a butkus quote from the API
func Butkus() (string, error) {
	resp, err := http.Get("https://butkus.xyz/api/quote")
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), err
}
