package juliacore

import (
	"math/rand"
	"strings"
)

// A CombinationResponder takes one from the inner slice for each element in the outer slice of Responses and combines, with the target of the response inserted at TargetIndex.
// The MessageType field indicates what type the response should be. This should be set in the configuration to be something like PRIVMSG or ACTION, to use an IRC example.
type CombinationResponder struct {
	Responses   [][]string `json:"responses"`
	TargetIndex int        `json:"targetindex"`
	MessageType string     `json:"messagetype"`
}

func (cr *CombinationResponder) Response(str, user string) (Response, error) {
	var resp []string
	for i, a := range cr.Responses {
		if i == cr.TargetIndex {
			resp = append(resp, user)
		}
		resp = append(resp, a[rand.Intn(len(a))])
	}
	return Response{Message: strings.Join(resp, " "), Type: cr.MessageType}, nil
}
