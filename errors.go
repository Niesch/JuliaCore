package juliacore

import (
	"errors"
)

var ErrNoWeather = errors.New("Kunde inte hitta vädret. Stavade du rätt?")
var ErrNoMatch = errors.New("Ingen matchande handling funnen.")
