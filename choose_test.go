package juliacore

import (
	"strings"
	"testing"
)

func TestChoose(t *testing.T) {
	j := NewJulia()
	err := j.LoadConf(strings.NewReader(`{"config": {
	    "choice_separator": "or"
	}}`))
	if err != nil {
		t.Errorf(err.Error())
	}
	res := j.Choose("cats or dogs")
	if res != "cats" && res != "dogs" {
		t.Errorf("expected cats or dogs, got %s", res)
	}
	res = j.Choose("cats OR dogs")
	if res != "cats" && res != "dogs" {
		t.Errorf("expected cats or dogs, got %s", res)
	}
}
