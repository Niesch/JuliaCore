package juliacore

import (
	"math/rand"
	"strings"
)

// Splits input on sep and returns a random choice.
func (j *Julia) Choose(in string) string {
	ins := strings.Split(strings.TrimSpace(in), " ")
	for i, e := range ins {
		if strings.EqualFold(e, j.Conf.ChoiceSep) {
			ins[i] = j.Conf.ChoiceSep
		}
	}
	s := strings.Split(strings.Join(ins, " "), j.Conf.ChoiceSep)
	return strings.TrimSpace(s[rand.Intn(len(s))])
}
